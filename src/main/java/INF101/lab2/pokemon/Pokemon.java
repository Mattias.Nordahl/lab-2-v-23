package INF101.lab2.pokemon;

import java.lang.Math;
import java.util.Random;

public class Pokemon implements IPokemon {

    private String name;
    private int healthPoints;
    private int maxHealthPoints;
    private int strength;
    private Random random;


    public Pokemon(String name) {
    
    this.name = name;
    this.random = new Random();
    this.healthPoints = (int) Math.abs(Math.round(100 + 10 * random.nextGaussian()));
    this.maxHealthPoints = this.healthPoints;
    this.strength = (int) Math.abs(Math.round(20 + 10 * random.nextGaussian()));

    
    }

    public String getName() {
        return name;
    }

    @Override
    public int getStrength() {
        return strength;

    }

    @Override
    public int getCurrentHP() {
        return healthPoints;

    }

    @Override
    public int getMaxHP() {
        return maxHealthPoints;

    }

    public boolean isAlive() {
        if(healthPoints > 0) {
            return true;
        }
        return false; 
        
    }

    @Override
    public void attack(IPokemon target) {
        Random random = new Random();
        int damageInflicted = (int) (this.strength + this.strength / 2 * random.nextGaussian());
        System.out.println(this.name + " attacks " + target.getName() + ".");
        target.damage(damageInflicted);
        if (target.getCurrentHP() <= 0) {
            System.out.println(target.getName() + " is defeated by " + this.name + ".");
        }
    }
    

    @Override
    public void damage(int damageTaken) {
        if (damageTaken < 0) {
            System.out.println("Negative damage is not allowed.");
            return;
        }
        healthPoints = Math.max(0, healthPoints - damageTaken);
        System.out.println(name + " takes " + damageTaken + " damage and is left with " + healthPoints + "/" + maxHealthPoints + " HP");
    }

    @Override
    public String toString() {
        return name + " HP: (" + healthPoints + "/" + maxHealthPoints + ") STR: " + strength;
    }

}
